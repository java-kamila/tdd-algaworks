package com.tddessencial;

import com.tddessencial.desconto.*;

// Pattern Test Data Builder
public class PedidoBuilder {

    private Pedido instancia;

    public PedidoBuilder() {
        CalculadoraFaixaDesconto calculadoraFaixaDesconto =
                new CalculadoraDescontoTerceiraFaixa(
                        new CalculadoraDescontoSegundaFaixa(
                                new CalculadoraDescontoPrimeiraFaixa(
                                        new CalculadoraSemDesconto(null))));

        instancia = new Pedido(calculadoraFaixaDesconto);
    }


    public PedidoBuilder comItem(double valorUnitario, int quantidade) {
        instancia.adicionarItem(
                new ItemPedido("Pedido gerado",
                        valorUnitario,
                        quantidade));
        return this;
    }

    public Pedido construir() {
        return instancia;
    }

}
