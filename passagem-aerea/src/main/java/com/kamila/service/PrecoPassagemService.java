package com.kamila.service;

import com.kamila.model.Passageiro;
import com.kamila.model.Voo;

public class PrecoPassagemService {

    public double calcular(Passageiro passageiro, Voo voo) {
        return passageiro.getTipo().getCalculadora().calcular(voo);
    }

}
