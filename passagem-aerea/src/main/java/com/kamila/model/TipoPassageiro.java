package com.kamila.model;

import com.kamila.service.CalculadoraPrecoPassagem;
import com.kamila.service.CalculadoraPrecoPassagemGold;
import com.kamila.service.CalculadoraPrecoPassagemSilver;

// O tipo de passageiro é o que determina a implementação
public enum TipoPassageiro {

    GOLD(new CalculadoraPrecoPassagemGold()),
    SILVER(new CalculadoraPrecoPassagemSilver());

    CalculadoraPrecoPassagem calculadoraPrecoPassagem;

    TipoPassageiro(CalculadoraPrecoPassagem calculadoraPrecoPassagem) {
        this.calculadoraPrecoPassagem = calculadoraPrecoPassagem;
    }

    public CalculadoraPrecoPassagem getCalculadora() {
        return this.calculadoraPrecoPassagem;
    }

}
