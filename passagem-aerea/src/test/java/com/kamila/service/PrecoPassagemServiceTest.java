package com.kamila.service;

import com.kamila.model.Passageiro;
import com.kamila.model.TipoPassageiro;
import com.kamila.model.Voo;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrecoPassagemServiceTest {

    private PrecoPassagemService precoPassagemService;

    @Before
    public void setup() {
        precoPassagemService = new PrecoPassagemService();
    }

    private void assertValorPassagem(Passageiro passageiro, Voo voo, double esperado) {
        double valor = precoPassagemService.calcular(passageiro, voo);
        assertEquals(esperado, valor, 0.0001);
    }

    // Teste básico para criação da classe (baby steps)
    @Test
    public void deveCriarPrecoPassagemService() throws Exception {
        PrecoPassagemService precoPassagemService = new PrecoPassagemService();
    }

    @Test
    public void devePermitirChamarCalculoDoValor() throws Exception {
        Passageiro passageiro = new Passageiro("João", TipoPassageiro.GOLD);
        Voo voo = new Voo("São Paulo", "Rio de Janeiro", 100.0);
        double valor = precoPassagemService.calcular(passageiro, voo);
    }

}
