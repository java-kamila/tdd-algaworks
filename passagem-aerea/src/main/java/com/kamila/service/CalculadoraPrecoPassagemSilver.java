package com.kamila.service;

import com.kamila.model.Voo;

public class CalculadoraPrecoPassagemSilver implements CalculadoraPrecoPassagem {

    @Override
    public double calcular(Voo voo) {
        if (voo.getPreco() > 700) {
            return calcularValorAbaixoDoLimite(voo);
        }
        return calcularValorAcimaDoLimite(voo);
    }

    private double calcularValorAbaixoDoLimite(Voo voo) {
        return voo.getPreco() * 0.9;
    }

    private double calcularValorAcimaDoLimite(Voo voo) {
        return voo.getPreco() * 0.94;
    }

}
