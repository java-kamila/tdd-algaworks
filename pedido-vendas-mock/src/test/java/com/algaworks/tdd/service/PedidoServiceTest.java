package com.algaworks.tdd.service;

import com.algaworks.tdd.email.NotificadorEmail;
import com.algaworks.tdd.model.Pedido;
import com.algaworks.tdd.model.StatusPedido;
import com.algaworks.tdd.model.builder.PedidoBuilder;
import com.algaworks.tdd.repository.PedidoRepository;
import com.algaworks.tdd.sms.NotificadorSms;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class PedidoServiceTest {

    private PedidoService pedidoService;
    private Pedido pedido;

    @Mock
    private PedidoRepository pedidoRepository;

    @Mock
    private NotificadorEmail notificadorEmail;

    @Mock
    private NotificadorSms notificadorSms;

    @Before
    public void setup() {
        // Inicialize nessa classe de teste as instâncias anotadas com @Mock
        MockitoAnnotations.initMocks(this);

        List<AcaoLancamentoPedido> acoes = Arrays.asList(pedidoRepository, notificadorEmail, notificadorSms);

        pedidoService = new PedidoService(pedidoRepository, acoes);

        pedido = new PedidoBuilder()
                .comValor(100.0)
                .para("João", "joao@joao.com", "9999-0000")
                .construir();

    }

    @Test
    public void deveCalcularOImposto() {
        double imposto = pedidoService.lancar(pedido);
        assertEquals(10.0, imposto, 0.0001);
    }

    @Test
    public void deveSalvarPedidoNoBancoDeDados() {
        pedidoService.lancar(pedido);
        // Verifica se o método executar foi chamado
        Mockito.verify(pedidoRepository).executar(pedido);
    }

    @Test
    public void deveNotificarPorEmail() {
        pedidoService.lancar(pedido);
        Mockito.verify(notificadorEmail).executar(pedido);
    }

    @Test
    public void deveNotificarPorSms() {
        pedidoService.lancar(pedido);
        Mockito.verify(notificadorSms).executar(pedido);
    }

    @Test
    public void devePagarPedidoPendente() {
        Long codigo = 135L;

        Pedido pedidoPendente = new Pedido();
        pedidoPendente.setStatus(StatusPedido.PENDENTE);
        when(pedidoRepository.buscarPeloCodigo(codigo))
                .thenReturn(pedidoPendente);

        Pedido pedidoPago = pedidoService.pagar(codigo);

        assertEquals(StatusPedido.PAGO, pedidoPago.getStatus());
    }

    @Test(expected = StatusPedidoInvalidoException.class)
    public void deveNegarPagamento() {
        Long codigo = 135L;

        Pedido pedidoPago = new Pedido();
        pedidoPago.setStatus(StatusPedido.PAGO);
        when(pedidoRepository.buscarPeloCodigo(codigo))
                .thenReturn(pedidoPago);

        pedidoService.pagar(codigo);
    }

}
