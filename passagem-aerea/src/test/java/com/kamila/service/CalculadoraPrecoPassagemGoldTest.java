package com.kamila.service;

import com.kamila.model.Passageiro;
import com.kamila.model.TipoPassageiro;
import com.kamila.model.Voo;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculadoraPrecoPassagemGoldTest {

    private PrecoPassagemService precoPassagemService;

    @Before
    public void setup() {
        precoPassagemService = new PrecoPassagemService();
    }

    private void assertValorPassagem(Passageiro passageiro, Voo voo, double esperado) {
        double valor = precoPassagemService.calcular(passageiro, voo);
        assertEquals(esperado, valor, 0.0001);
    }

    @Test
    public void deveCalcularPrecoParaPassageiroGold_ComValorAbaixoDoLimite() throws Exception {

        Passageiro passageiro = new Passageiro("João", TipoPassageiro.GOLD);
        Voo voo = new Voo("São Paulo", "Rio de Janeiro", 100.0);

        assertValorPassagem(passageiro, voo, 90.0);
    }

    @Test
    public void deveCalcularPrecoParaPassageiroGold_ComValorAcimaDoLimite() throws Exception {

        Passageiro passageiro = new Passageiro("João", TipoPassageiro.GOLD);
        Voo voo = new Voo("São Paulo", "Rio de Janeiro", 600.0);

        assertValorPassagem(passageiro, voo, 510.0);
    }


}
