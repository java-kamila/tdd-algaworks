package com.kamila.service;

import com.kamila.model.Passageiro;
import com.kamila.model.TipoPassageiro;
import com.kamila.model.Voo;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculadoraPrecoPassagemSilverTest {

    private PrecoPassagemService precoPassagemService;

    @Before
    public void setup() {
        precoPassagemService = new PrecoPassagemService();
    }

    private void assertValorPassagem(Passageiro passageiro, Voo voo, double esperado) {
        double valor = precoPassagemService.calcular(passageiro, voo);
        assertEquals(esperado, valor, 0.0001);
    }

    @Test
    public void deveCalcularPrecoParaPassageiroSilver_ComValorAbaixoDoLimite() throws Exception {
        Passageiro passageiro = new Passageiro("Maria", TipoPassageiro.SILVER);
        Voo voo = new Voo("São Paulo", "Rio de Janeiro", 100.0);

        assertValorPassagem(passageiro, voo, 94.0);
    }

    @Test
    public void deveCalcularValorPssagemParaPassageiroSilver_ComValorAcimaDoLimite() {
        Passageiro passageiro = new Passageiro("Maria", TipoPassageiro.SILVER);
        Voo voo = new Voo("São Paulo", "Rio de Janeiro", 800.0);

        assertValorPassagem(passageiro, voo, 720.0);
    }
}