package com.algaworks.tdd.service;

import com.algaworks.tdd.model.Pedido;
import com.algaworks.tdd.model.StatusPedido;
import com.algaworks.tdd.repository.PedidoRepository;

import java.util.List;

public class PedidoService {

    private List<AcaoLancamentoPedido> acoes;
    private PedidoRepository pedidoRepository;

    public PedidoService(PedidoRepository pedidoRepository, List<AcaoLancamentoPedido> acoes) {
        this.acoes = acoes;
        this.pedidoRepository = pedidoRepository;
    }

    public double lancar(Pedido pedido) {
        double imposto = pedido.getValor() * 0.1;

        acoes.forEach(a -> a.executar(pedido));

        return imposto;
    }

    public Pedido pagar(Long codigoPedido) {
        final Pedido pedido = pedidoRepository.buscarPeloCodigo(codigoPedido);

        if (!pedido.getStatus().equals(StatusPedido.PENDENTE)) {
            // Deve estar pendente para aceitar pagamento
            throw new StatusPedidoInvalidoException();
        }
        pedido.setStatus(StatusPedido.PAGO);
        return pedido;
    }
}
