package com.tddessencial.desconto;

public abstract class CalculadoraFaixaDesconto {

    private CalculadoraFaixaDesconto proximo;

    public CalculadoraFaixaDesconto(CalculadoraFaixaDesconto proximo) {
        this.proximo = proximo;
    }

    public double getDesconto(double valorTotal) {
        double desconto = calcular(valorTotal);

        if (desconto == -1) {
            return proximo.getDesconto(valorTotal);
        }

        return desconto;
    }

    protected abstract double calcular(double valorTotal);

}
