# TDD Essencial

## Ciclo vermelho-verde-refatora

![Ciclo vermelho-verde-refatora](_images/fluxo-tdd.png)

Iniciando o desenvolvimento a partir de um teste que falha.
Após isso desenvolver o suficiente para o teste passar.
Refatorar o código.

### Baby Steps
Dar passos pequenos por vez para criação de testes e funcionalidades. Indicado para algo mais complexo ou não habitual, necessária análise do cenário. Par algo mais simples é possível dar passos maiores.

<b> Requisitos para aplicação de desconto [pedido-vendas]</b>
![Regra negócio de Pedido Vendas](_images/pedido-vendas-desconto-regra.png)

### Testes antes do código
Ao fazer os testes depois, o desenvolvedor dedica um bom tempo para fazer o código e outro bom tempo para criar os testes.
Abordagem que possibilita menos debug para varredura de erros, o código evolui aos poucos, permite desenvolvimento mais confiável e tende a requisitar menos tempo do que o método de fazer os testes apenas depois.

Sugestão de ferramenta <b>Hamcrest</b> https://github.com/hamcrest/JavaHamcrest. Um framework para escrever objetos matcher, mais informações na página [tutorial](https://code.google.com/archive/p/hamcrest/wikis/Tutorial.wiki).

### O código do teste é importante

Testa o código de produção. Documenta a utilização de funcionalidades.
Possui três partes:
 - preparar cenário
 - realizar ação
 - validar resultado esperado

Deve-se evitar código duplicado, inclusive nos testes.

O nome do teste deve representar claramente o que ele faz, não precisa ser um nome curto. Há convenções de separação de palavras por camelCase, utilização de underscore, ou padrão acordado pela equipe.

Padrão Test Data Builder, interessante para criação de objetos para o cenário. Em [PedidoBuilder](pedido-vendas/src/test/java/com/tddessencial/PedidoBuilder.java).

Deve se evitar inserir mais de um cenário e asserções em um mesmo método de teste. Caso as asserções estejam relacionadas é possível estarem no mesmo método, necessária análise.

Deve-se testar as Exceções.

### Identificando responsabilidades de uma classe
Exemplo de caso de uso.
<b> Requisitos para aplicação de desconto [passagem-aerea]</b>
![Passagem Aerea Requisitos](_images/requisitos-passagem-aerea.png)


### Usando Mock Objetos
Utilizamos o Mockito para criação de objetos <b>simulados</b> de teste.
![Pedido Vendas Requisitos](_images/requisitos-pedido-vendas.png)

Exemplos de utilização do Mockito em [PedidoServiceTest.java](pedido-vendas-mock/src/test/java/com/algaworks/tdd/service/PedidoServiceTest.java).

### Conclusão

 - Use TDD no seu próprio projeto. Assim fica mais natural a cada dia.
 - É uma opção do desenvolvedor!
 - Estude mais Orientação a Objetos e Boas práticas. TDD auxilia, mas não codifica pelo desenvolvedor. Teste difícil de ser realizado dá indícios de um código de má qualidade.

## Developer
Kamila Serpa

[1]: https://www.linkedin.com/in/kamila-serpa/
[2]: https://gitlab.com/java-kamila

[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)][1]
[![gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)][2]
