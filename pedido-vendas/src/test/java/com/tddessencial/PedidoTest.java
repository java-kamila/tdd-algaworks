package com.tddessencial;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PedidoTest {

    private PedidoBuilder pedidoBuilder;

    @Before
    public void setup() {
        pedidoBuilder = new PedidoBuilder();
    }

    private void assertResumoPedido(double valorTotal, double desconto) {
        ResumoPedido resumoPedido = pedidoBuilder.construir().resumo();
        assertEquals(new ResumoPedido(valorTotal, desconto), resumoPedido);
    }

    @Test
    public void deveCalcularValorTotalEDescontoParaPedidoVazio() throws Exception {
        assertResumoPedido(0.0, 0.0);
    }

    @Test
    public void deveCalcularResumoParaUmItemSemDesconto() throws Exception {
        pedidoBuilder.comItem(5.0, 5);
        assertResumoPedido(25.0, 0.0);
    }

    @Test
    public void deveCalcularResumoParaDoisItensSemDesconto() throws Exception {
        pedidoBuilder
                .comItem(3.0, 3)
                .comItem(7.0, 3);

        assertResumoPedido(30.0, 0.0);
    }

    @Test
    public void deveAplicarDescontoNa1aFaixa() throws Exception {
        pedidoBuilder.comItem(20.0, 20);

        assertResumoPedido(400.0, 16.0);
    }

    @Test
    public void deveAplicarDescontoNa2aFaixa() throws Exception {
        pedidoBuilder
                .comItem(15.0, 30)
                .comItem(15.0, 30);

        assertResumoPedido(900.0, 54.0);
    }

    @Test
    public void deveAplicarDescontoNa3aFaixa() throws Exception {
        pedidoBuilder
                .comItem(15.0, 30)
                .comItem(15.0, 30)
                .comItem(10.0, 30);

        assertResumoPedido(1200.0, 96.0);
    }

    @Test(expected = QuantidadeItensInvalidaException.class)
    public void deveNaoAceitarPedidosComItensComQuantidadesNegativas() throws Exception {
        pedidoBuilder.comItem(0.0, -10);
    }

    @Test
    public void deveReceberMensagemExceptionAoNaoAceitarPedidosComItensComQuantidadesNegativas() throws Exception {
        try {
            pedidoBuilder.comItem(0.0, -10);
            fail("Deveria ter lançado a exception QuantidadeItensInvalidaException");
        } catch (Exception e) {
            String message = e.getMessage();
            assertEquals("Quantidade não pode ser negativa", message);
        }
    }

}
