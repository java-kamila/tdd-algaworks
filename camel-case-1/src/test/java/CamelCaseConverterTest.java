import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CamelCaseConverterTest {

    private CamelCaseConverter camelCaseConverter;

    @Before
    public void setup() {
        camelCaseConverter = new CamelCaseConverter();
    }

    @Test
    public void deveAplicarCamelCaseEmNomeUnico() throws Exception {
        CamelCaseConverter camelCaseConverter = new CamelCaseConverter();
        assertEquals("Lionel", camelCaseConverter.converter("lionel"));
    }

    @Test
    public void deveConverterNomeSimplesMisturadoMaiusculoEMinusculo() throws Exception {
        assertEquals("Ronaldo", camelCaseConverter.converter("rOnAlDo"));
    }

}