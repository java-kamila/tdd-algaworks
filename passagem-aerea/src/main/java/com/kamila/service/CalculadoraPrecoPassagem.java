package com.kamila.service;

import com.kamila.model.Voo;

public interface CalculadoraPrecoPassagem {

    public double calcular(Voo voo);

}
