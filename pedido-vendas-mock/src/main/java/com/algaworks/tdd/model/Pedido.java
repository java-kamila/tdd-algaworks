package com.algaworks.tdd.model;

public class Pedido {

    private double valor;
    private Cliente cliente;
    private StatusPedido statusPedido;

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public StatusPedido getStatus() {
        return statusPedido;
    }

    public void setStatus(StatusPedido status) {
        this.statusPedido = status;
    }
}
