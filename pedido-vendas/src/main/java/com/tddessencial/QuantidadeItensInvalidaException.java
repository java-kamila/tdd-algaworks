package com.tddessencial;

public class QuantidadeItensInvalidaException extends RuntimeException {

    public QuantidadeItensInvalidaException() {
        super("Quantidade não pode ser negativa");
    }
}
